- Egyéni edzéstípusok
- Edzések megosztása képekként
- Felhasználói felület és stabilitási továbbfejlesztések
- A „rendszerműveletek” bejelentés mostantól letiltható
- Fordítások frissítése

A változtatások részletesebb listájáért nézze meg a projekt tárolóját a Codebergen
