- Evezés hozzáadása
- Az Android 10 megcélozva

- A régebbi Android eszközökön történő GPX importálás és exportálás javítása
- A tempó hangos közlemény javítása
- Kisebb elrendezési javítások

- Kezdeti orosz fordítás hozzáadva
